+++
title = "Infrastructure IT"
lastmodifierdisplayname = "Lionel SCHRÖDER"
lastmodifieremail = "lionel.schroder@gmail.com"
weight = 30
+++

La première chose que j'ai installé à la maison est mon matériel informatique. Cet équipement est très utile également pour la mise en place de la domotique. L'investissement est donc partagé car il a plusieurs utilités.