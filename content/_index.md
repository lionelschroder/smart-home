+++ 
title = "My Smart Home" 
lastmodifierdisplayname = "Lionel SCHRÖDER"
lastmodifieremail = "lionel.schroder@gmail.com"
+++


# My Smart Home

Je réalise ce site car beaucoup de personnes m'ont demandé des informations à propos de la domotique que j'ai installée chez moi ou eu l'occasion de tester. 
C'est un bon moyen de partager les différentes connaissances acquises ainsi qu'une documentation personnelle qui me servira d'aide mémoire.

#### Pourquoi la domotique ?

Passionné par les nouvelles technologies et par l'informatique, la domotique est un bon moyen de combiner les deux. C'est aussi une bonne façon de pouvoir apprécier quelque chose de concret assez rapidement. On peut aussi mettre en pratique d'autres compétences (électricité, chauffage, bricolage, ...).

#### Quels sont les critères qui ont influencé mes choix ?

##### Mes besoins :
* **Intégration avec [HomeKit]({{%relref "homekit.md"%}}) d'Apple :** Étant équipés principalement de produits Apple, c'est un point très important car cela permet automatiquement de profiter des fonctionnalités qui y sont intégrées. On a toujours notre smartphone a portée de main et c'est lui qu'on utilisera en premier lieu pour interagir avec notre domotique (Raccourci centre de contrôle, Siri, ...)
* **Meilleure gestion du [chauffage]({{%relref "chauffage/_index.md"%}}) :** Quand le poêle à bois fonctionnait dans le salon mon ancien thermostat coupait le chauffage dans toute la maison
* **Meilleur confort :** Pouvoir combiner plusieurs actions, actions automatiques -> gain de temps
* **Sécurité :** Ma maison n'était pas équipée d'une alarme
* **Économie d'énergie**

##### Mes contraintes :
* **Pas de gros travaux :** Ma nouvelle maison a été achetée en très bon état et il n'était pas question de faire des trous partout ou de rainurer tous les murs. Donc privilégier les technologies sans fil sauf pour le réseau internet ou j'ai câblé le maximum d'appareils possibles
* **Prix raisonnable**
* **Open source** pour pouvoir adapter la solution si nécessaire. Sans devoir tout re-développer car manque de temps et privilégier les solutions largement utilisées pour une meilleure intégration
* **Éviter les protocoles fermés ou propriétaires** afin d'éviter d'être dépendant d'un seul fabricant avec toutes les contraintes que cela impliquerai
* **Importante communauté :** aide facile à trouver, meilleure garantie de longévité
