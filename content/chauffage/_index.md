+++
title = "Chauffage"
lastmodifierdisplayname = "Lionel SCHRÖDER"
lastmodifieremail = "lionel.schroder@gmail.com"
weight = 40
+++

## Introduction
### Situation initiale
* Ma maison est équipée d'un chauffage central avec chaudière à mazout ainsi que d'un poêle à bois dans la pièce principale
* Elle était équipée d'un thermostat classique  et chaque radiateur de vanne thermostatique "manuelle"
    * Il permettait une programmation horaire
    * Il était situé dans la pièce principale (la même que le poêle à bois) c'est donc dans cette pièce que le capteur de température se situait et la maison était considérée comme une seule zone de chauffage
    * Il y avait un mode "vanne/manuel" qui permettait de chauffer en ignorant la température mesurée et de régler chaque vanne en fonction de la pièce à chauffer

### Pourquoi changer de solution ?
* Intégrer le chauffage dans la domotique de la maison, c'était auparavant une solution isolée
* Permettre de gérer le chauffage de chaque pièce indépendamment sans devoir le faire manuellement à chaque vanne. C'est toujours sympa d'être sûr que la salle de bains sera à la bonne température pour le bain de bébé
* Quand le poêle à bois fonctionnait dans le salon mon ancien thermostat coupait le chauffage dans toute la maison car il y faisait assez chaud et la seule solution était de tout contrôler manuellement
* Faire des économies

La raison principale est donc pour plus de confort : diminuer les actions manuelles et le risque d'oubli (allumer ou éteindre une vanne). Ayant emménagé peu de temps avant de changer de thermostat je n'ai pas assez de données sur mon ancienne solution afin de chiffrer les éventuelles économies réalisées.

### Pistes envisagées
* Netatmo : J'ai eu quelques mauvais échos concernant leur thermostat, il était sortit depuis plusieurs années ce qui me semblait être un inconvénient comme nous ne sommes qu'au début de ces solutions (design et intégration aux autres solutions déjà dépassés ?). Les vannes connectées venaient également tout juste de sortir après beaucoup de retard, pas encore disponibles ou pas encore de reviews
* Nest : Probablement le plus connu mais avait plus l'air d'être fait pour un appartement (une seule zone, pas de vannes connectées, ...)
* Autre solution plus "custom". Il existe par exemple des vannes Z-Wave qui auraient été compatibles avec mon système domotique mais je cherchais plutôt une solution complète (thermostat + vannes) afin d'éviter un maximum de devoir intégrer plusieurs devices et élaborer des scénarios complexes dans Jeedom pour avoir une solution de chauffage complète alors que d'autres solutions le proposent déjà et que l'écart de prix n'est pas énorme

## Tado°
Tado est une entreprise allemande qui commercialise un thermostat et des vannes connectées. C'est une nouvelle entreprise mais le produit est sortit depuis plusieurs années.

Plus d'infos : https://www.tado.com/be/

### Pourquoi Tado
Mon choix s'est assez vite tourné vers Tado pour plusieurs raisons :

* Impression de fiabilité (marque allemande, reviews positives)
* Diverses intégrations (Homekit, IFTTT, Alexa, ...) même si Jeedom m'aurait permis de l'intégrer autrement, c'est toujours mieux quand c'est directement intégré 
* Design sympa
* Prix intéressant en combinant des packs, utilisation possible d'éco-chèques
* Les vannes permettent du multi-zones pour chaque pièce de la maison
* L'installation parait simple et compatible avec mon système de chauffage

### Installation

### Coût de l'installation
Voir BoM

### Avis sur l'usage au quotidien
Cela fait maintenant plus d'un an que j'utilise Tado.

#### Avantages

#### Inconvénients

####  What's next ?
