+++ 
title = "Jeedom" 
lastmodifierdisplayname = "Lionel SCHRÖDER"
lastmodifieremail = "lionel.schroder@gmail.com"
weight = 10
+++

![jeedom](https://www.jeedom.com/site/logo.png)
J'ai choisi Jeedom comme OS domotique.

## Présentation
Jeedom est un logiciel open source développé principalement par des français.

Plus d'infos : https://www.jeedom.com/site/fr/index.html

## Choix de Jeedom
Malgré d'autres solutions similaires disponibles, par exemple Domoticz, Jeedom semblait le plus abouti. C'est le seul que j'ai testé mais il correspond parfaitement à mes besoins. Il peut même faire un tas de choses que je n'utiliserai probablement pas.

* Open Source :
	* Gratuit : Attention l'OS est gratuit mais certains plug-ins, qu'ils soient officiels ou non sont payants (Pour un montant raisonable)
	* Possibilité de développer ses propres scripts ou plug-ins
* Compatibilité :
	* [HomeKit]({{%relref "homekit.md"%}})
	* Z-Wave qui est un protocole que la plupart de mes modules utilisent
* Matériel : Un simple Raspberry Pi suffit pour le faire tourner

## Installation
