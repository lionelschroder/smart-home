+++
title = "Bill of Materials"
lastmodifierdisplayname = "Lionel SCHRÖDER"
lastmodifieremail = "lionel.schroder@gmail.com"
weight = 60
+++

### Infrastructure IT
<table>
    <thead>
        <tr>
            <th>Catégorie</th>
            <th>équipement</th>
            <th>Prix</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="center" rowspan="4">Général</td>
            <td><a href="https://www.amazon.fr/gp/product/B00O95JW3I/ref=oh_aui_detailpage_o01_s01?ie=UTF8&psc=1" target="_blank">Armoire rack</a></td>
            <td align="right">105€</td>
            <td align="right" rowspan="4">Somme</td>
        </tr>
        <tr>
            <td><a href="https://www.amazon.fr/gp/product/B00ZQ0W7KU/ref=oh_aui_detailpage_o01_s02?ie=UTF8&psc=1" target="_blank">Etagère rack</a></td>
            <td align="right">22€</td>
        </tr>
        <tr>
            <td><a href="https://www.amazon.fr/dp/B002TANS0I/ref=pe_3044141_189395771_TE_dp_3" target="_blank">Onduleur APC</a></td>
            <td align="right">82€</td>
        </tr>
        <tr>
            <td><a href="https://www.amazon.fr/gp/product/B01B9U1W2O/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1" target="_blank">Panneau de brassage</a></td>
            <td align="right">35€</td>
        </tr>
        <tr>
            <td align="center" rowspan="5">Réseau</td>
            <td><a href="https://www.amazon.fr/gp/product/B0722XW4PD/ref=oh_aui_detailpage_o01_s03?ie=UTF8&psc=1" target="_blank">Switch Tplink POE gigabit 16 ports</a></td>
            <td align="right">146€</td>
        </tr>
        <tr>
            <td><a href="https://www.coolblue.be/fr/produit/338433/tp-link-archer-c7.html" target="_blank">Routeur WiFi Tplink Archer C7</a> (2X)</td>
            <td align="right">158€</td>
        </tr>
        <tr>
            <td><a href="https://www.coolblue.be/fr/produit/807078/ubiquiti-unifi-ap-ac-pro-e.html" target="_blank">Point d'accès WiFi Ubiquiti UniFi AP-AC-PRO-E</a></td>
            <td align="right">127€</td>
        </tr>
        <tr>
            <td><a href="https://www.amazon.fr/gp/product/B00EOTHF4W/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1" target="_blank">Câble RJ45 CAT6 100M (Avec embouts et pince)</a> (2X)</td>
            <td align="right">104€</td>
        </tr>
        <tr>
            <td><a href="https://www.amazon.fr/gp/product/B00EOTHF4W/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1" target="_blank">Câble RJ45 CAT6 100M (Avec embouts et pince)</a> (2X)</td>
            <td align="right">104€</td>
        </tr>
        <tr>
            <td align="center" rowspan="3">NAS</td>
            <td><a href="https://www.materiel.net/produit/201710170017.html" target="_blank">Synology NAS DS215J (Remplacé par DS218J)</a></td>
            <td align="right">189€</td>
        </tr>
        <tr>
            <td><a href="https://www.materiel.net/produit/201710170017.html" target="_blank">HDD 4TB</a> (2X)</td>
            <td align="right">334€</td>
        </tr>
        <tr>
            <td><a href="https://www.materiel.net/produit/201708210059.html" target="_blank">Remplacement HHD défectueux</a></td>
            <td align="right">136€</td>
        </tr>
    </tbody>
</table>